const { query } = require("express");
const express = require("express");
const router = express.Router();
const utils = require("../utils");

// display all data of employee
router.get("/:id", (request, response) => {
  const { id } = request.params;

  const connection = utils.openConnection();

  const statement = `
        select * from Emp where
        empid = '${id}'
      `;
  connection.query(statement, (error, result) => {
    connection.end();
    if (result.length > 0) {
      console.log(result.length);
      console.log(result);
      response.send(utils.createResult(error, result));
    } else {
      response.send("user not found !");
    }
  });
});

router.post("/add", (request, response) => {
  const { name, salary, age } = request.body;

  const connection = utils.openConnection();
  console.log(connection);
  const statement = `
        insert into Emp
          ( name,salary,age)
        values
          ( '${name}',${salary},${age})
      `;
  connection.query(statement, (error, result) => {
    connection.end();
    response.send(utils.createResult(error, result));
  });
});

router.put("/:id", (request, response) => {
  const { id } = request.params;
  const { salary } = request.body;

  const statement = `
    update Emp
    set
      salary=${salary}
    where
      empid = '${id}'
  `;
  const connection = utils.openConnection();
  connection.query(statement, (error, result) => {
    connection.end();
    console.log(statement);

    response.send(utils.createResult(error, result));
  });
});


router.delete("/:id", (request, response) => {
  const { id } = request.params;
  const statement = `
    delete from Emp
    where
      empid = '${id}'
  `;
  const connection = utils.openConnection();
  connection.query(statement, (error, result) => {
    connection.end();
    console.log(statement);
    response.send(utils.createResult(error, result));
  });
});

//Show All details

router.put("/show", (request, response) => {
  // const { id } = request.params;
  // const { salary } = request.body;

  const statement = 
  //`
  //   update Emp
  //   set
  //     salary=${salary}
  //   where
  //     empid = '${id}'
  // `;
  ` select * from Emp`;

  const connection = utils.openConnection();
  connection.query(statement, (error, result) => {
    connection.end();
    console.log(statement);

    response.send(utils.createResult(error, result));
  });
});

module.exports = router;
